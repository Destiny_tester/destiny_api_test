from utils.load_file import load_yaml_file
from utils.common import req
from utils.aver import assert_data as aver
import pytest, allure






@allure.epic('针对单接口的测试V1.1')
@allure.feature('对象模块·我的信息')
class Test_person_info:

    @allure.title('test')
    @allure.story("用例--填写符合要求的数据, 修改用户信息成功！")
    @pytest.mark.flaky(reruns=2, reruns_delay=1)
    @pytest.mark.parametrize('data', load_yaml_file(r'data\PersonInfo.yaml'))
    def deit_person_info(data):
        res = req(method=data['method'], api_url=data['api_url'], header=data['headers'], body=data['body'])
        allure.dynamic.title(data[4])
        allure.attach(str(data), '请求参数', allure.attachment_type.JSON)
        allure.attach(res.text, '响应结果', allure.attachment_type.JSON)
        aver(res)
        



if __name__ == '__main__':
    pytest.main(['-s', r'case\test_PersonInfo.py'])








