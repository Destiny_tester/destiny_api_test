# -*- coding: utf-8 -*-


import pymysql
from config import DBCONFIG

    
class use_mysql:

    def query(self, val):
        with pymysql.connect(**DBCONFIG) as db:
            cur = db.cursor()
            cur.execute(val)
            results = cur.fetchall()
            return results

    def update(self, sql_val):
        with pymysql.connect(**DBCONFIG) as db:
            try:
                cur = db.cursor()
                cur.execute(sql_val)
                db.commit()
                return True
            except:
                db.rollback()
                return False
            










