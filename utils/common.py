import requests
from utils.load_file import IniReader



def req(method, api_url, header, body, files=None):
    """ requests方法的简单改造 """
    env = IniReader('conf\env_ip.ini').get_value('env', 'api_test_dev')
    test_url = ''.join([env, api_url])
    res = requests.request(method=method, url=test_url, headers=header, json=body, files=files)
    return res



def replace(pub_data, data):
    for p_key, p_val in pub_data.items():
        if data:
            for key, val in data.items():
                if val == p_key:
                    try:
                        key = p_val
                    except:
                        return False


def exeract(datas, dicts, res):
    """ 
    该方法用于提取公共变量
    data: 用例
    dicts: 利用fixture方法定义一个全局变量, 返回一个字典
    res: 响应结果
    """
    from jsonpath import jsonpath    
    if 'extract' in datas:   # 判断用例中是否有'extract'的值，如果有则执行以下代码
        for key, value in datas['extract'].items():    # 遍历测试用例中的'extract', 用字典的内置函数items来获取它的key和value值
            exec(f"{key} = {value}", locals(), dicts)     # 使用exec()方法来执行yaml文件中的jsonpath语句并赋值给key, 最后添加到全局变量dicts中


def replace(data, dicts):
    """
    该方法用于引用需要的公共变量
    data: 用例
    dicts: 从dicts字典中找可以替换的参数
    """
    if data['body'] != None:
        for akey, aval in data['body'].items():
            if aval in dicts:
                for akeys, avals in dicts.items():
                    data['body'][akey] = dicts[akeys]




def extract():
    pass









