# -*- coding: utf-8 -*-


import redis
from config import REDIS_CONFIG

def set_redis(key, val):
    try:
        r = redis.Redis(**REDIS_CONFIG)
        r.set(key, val)
        r.close()
        return True
    except:
        return False


def get_redis(key):
    try:
        r = redis.Redis(**REDIS_CONFIG)
        result = r.get(key)
        r.close()
        result if result is None else result.decode('utf-8')
        return result
    except:
        return False


def del_redis(key):
    try:
        r = redis.Redis(**REDIS_CONFIG)
        r.delete(key)
        r.close()
        return True
    except:
        return False


def del_by_redis_userid(val):
    try:
        r = redis.Redis(**REDIS_CONFIG)
        for k in r.keys():
            v = str(r.get(k), encoding='utf-8')
            if v == str(val):
                del_redis(v)
        r.close()
        return True
    except:
        return False    



