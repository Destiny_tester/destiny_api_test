






import configparser
import yaml



def load_yaml_file(file_path):
    """ 读取yaml文件 """
    with open(file_path, 'r', encoding='utf-8') as f:
        return yaml.load(f, Loader=yaml.FullLoader)
    



class IniReader:
    """ 用于读取ini文件的方法 """
    def __init__(self, file_path):
        """
        file_path: 需要读取ini文件的位置
        """
        self.file_path = file_path
        self.config = configparser.ConfigParser()
        self.config.read(file_path, encoding='UTF-8')

    def get_value(self, key, value):
        """
        key: 参数的名称
        value: 参数的值
        """
        value = self.config.get(key, value)
        return value






